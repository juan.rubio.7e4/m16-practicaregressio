import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-2, 2, 10)
y = x # Funció linial

y2 = x**2 # Funció linial
y3 = x**3 # Funció linial

plt.plot(x, y, "-", label='Linial')  # Imprimim els punts
plt.plot(x, y2, "o", label='Quadratica')  # Imprimim els punts
plt.plot(x, y3, ".", label='Cubica')  # Imprimim els punts

plt.xlabel('Valors de x')  # Add an x-label to the axes.
plt.ylabel('Valors de y')  # Add a y-label to the axes.
plt.title("Funcions")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.show()