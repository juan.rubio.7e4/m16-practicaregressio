import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn import linear_model

import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as stats
import re

matplotlib.style.use('ggplot')

try:
    from urllib.request import Request, urlopen  # Python 3
except ImportError:
    from urllib2 import Request, urlopen  # Python 2

# Load laptops data set

url = "https://gitlab.com/juan.rubio.7e4/m16-practicaregressio/-/raw/master/data/laptops.csv"

# Aquesta part s'utilitza per poder accedir a la URL de GitLab (no accepta connexions que no vinguin de un browser)
req = Request(url)
req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0')
content = urlopen(req)

laptops = pd.read_csv(content, index_col=0, encoding="UTF-16")

def extract_ram(x):
    match = re.search(r"(\d+)GB", x )
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la RAM", x)

def extract_cpu(x):
    match = re.search("(\d+(\.\d+)?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure la freqüència de la CPU")

def extract_res(x):
    match = re.search("(\\d{4})", x)
    if match:
        return int(match.groups()[0])
    else:
        raise Exception("Error al extreure la resolució de pantalla")

def extract_weight(x):
    match = re.search("(\\d(\\.\\d+)?)kg", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al extreure el pes")

def extract_Memory(x):
    match = re.findall(r"(\d+)([GT])B (HDD)?", x)
    midaHDD = 0
    midaSSD = 0
    if match:
        for i in match:
            local_mida = int(i[0])
            if i[1] == "T":
                local_mida *= 1024
            if i[2] == "HDD":
                midaHDD += local_mida
            else:
                midaSSD += local_mida
        return midaHDD, midaSSD
    else:
        raise Exception("Error al extreure la memòria", x)

laptops["Ram"] = laptops["Ram"].apply(extract_ram)
laptops["Cpu"] = laptops["Cpu"].apply(extract_cpu)
laptops["ScreenResolution"] = laptops["ScreenResolution"].apply(extract_res)
laptops["Weight"] = laptops["Weight"].apply(extract_weight)
laptops["MemoryHDD"], laptops["MemorySSD"] = zip(*laptops["Memory"].map(extract_Memory))


def entrenarLineal(caracteristiques):
    X = laptops.loc[:, caracteristiques]
    y = laptops["Price_euros"]
    x_train, x_test, y_train, y_test = train_test_split(
        X,y, test_size= 0.20, random_state=2020, shuffle = True)
    regression_model = linear_model.LinearRegression()
    regression_model.fit(x_train, y_train)
    print("Train: ", regression_model.score(x_train, y_train))
    print("Test: ", regression_model.score(x_test, y_test))

def entrenarPolinomial(caracterisitques):
    X = laptops.loc[:, caracterisitques]
    y = laptops["Price_euros"]
    x_train, x_test, y_train, y_test = train_test_split(
        X,y, test_size=0.20, random_state=2020, shuffle= True)
    polynomial_model = make_pipeline(PolynomialFeatures(degree= 2), LinearRegression())
    polynomial_model.fit(x_train, y_train)
    print("Train: " ,polynomial_model.score(x_train, y_train))
    print("Test: " , polynomial_model.score(x_test, y_test))

print("---LINEAR REGRESSION---")
print("\t\t---RAM---")
entrenarLineal(["Ram"])
print("\n\t\t---CPU---")
entrenarLineal(["Cpu"])
print("\n\t\t---RAM i CPU---")
entrenarLineal(["Ram", "Cpu"])
print("\n---RAM, CPU i SCREENRESOLUTION---")
entrenarLineal(["Ram", "Cpu", "ScreenResolution"])
print("\n---RAM, CPU, SCREENRESOLUTION i MEMORY---")
entrenarLineal(["Ram", "Cpu", "ScreenResolution", "MemoryHDD", "MemorySSD"])

print("\n---POLINOMIAL REGRESSION---")
print("\n\t\t---RAM i CPU---")
entrenarPolinomial(["Ram", "Cpu"])
print("\n---RAM, CPU i SCREENRESOLUTION---")
entrenarPolinomial(["Ram", "Cpu", "ScreenResolution"])
print("\n---RAM, CPU, SCREENRESOLUTION i MEMORY---")
entrenarPolinomial(["Ram", "Cpu", "ScreenResolution", "MemoryHDD", "MemorySSD"])
print()

